package com.jfinal.render;

import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import net.dreamlu.helpers.JadeHelper;
import de.neuland.jade4j.Jade4J;
import de.neuland.jade4j.Jade4J.Mode;
import de.neuland.jade4j.JadeConfiguration;
import de.neuland.jade4j.template.JadeTemplate;

/**
 * 返回jade xml
 * @author 春梦
 *
 */
public class JadeXmlRender extends JadeRender {

	private static final long serialVersionUID = 1701376701891462950L;
	private transient static final String contentType = "text/xml; charset=" + getEncoding();
	
	public JadeXmlRender(String view) {
		super(view);
	}

	@Override
	public void render() {
		response.setContentType(contentType);
		JadeConfiguration config = getConfiguration();
        Enumeration<String> attrs = request.getAttributeNames();
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("jTag", new JadeHelper());
        while (attrs.hasMoreElements()) {
            String attrName = attrs.nextElement();
            model.put(attrName, request.getAttribute(attrName));
        }
        PrintWriter writer = null;
        try {
        	config.setMode(Mode.XML);
            JadeTemplate template = config.getTemplate(view);
            writer = response.getWriter();
            Jade4J.render(template, model, writer);
        } catch (Exception e) {
            throw new RenderException(e);
        }
        finally {
            if (writer != null)
                writer.close();
        }
	}
}
