package com.jfinal.ext.plugin.redis;

import net.dreamlu.utils.ConfigUtil;

import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;

import com.baidu.bae.api.util.BaeEnv;
import com.jfinal.plugin.IPlugin;

/**
 * BAE Redis插件 BAE 插件模式一直空指针，无解啊
 * @author L.cm
 * @date Jul 27, 2013 8:29:41 PM
 */
public class RedisPlugin implements IPlugin {
	
	private static final Logger log = Logger.getLogger(RedisPlugin.class);

	private Jedis jedis;
	
	static String server = BaeEnv.getBaeHeader(BaeEnv.BAE_ENV_ADDR_REDIS_IP);
	static String port_str = BaeEnv.getBaeHeader(BaeEnv.BAE_ENV_ADDR_REDIS_PORT);
	static int port = Integer.parseInt(port_str);
	static String host = server + ":" + port;
	static String user = BaeEnv.getBaeHeader(BaeEnv.BAE_ENV_AK);
	static String password = BaeEnv.getBaeHeader(BaeEnv.BAE_ENV_SK);
	static String databaseName = ConfigUtil.get("jedis_db");
	
	public boolean start() {
		jedis = new Jedis(server, port);
		jedis.connect();
		jedis.auth(user + "-" + password + "-" + databaseName);
		RedisKit.init(jedis);
		log.info("jedis start......");
		log.info("jedis dbSize......");
		return true;
	}
	
	public boolean stop() {
		jedis = null;
		return true;
	}
}
