package com.jfinal.ext.plugin.redis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.dreamlu.utils.ConfigUtil;

import org.apache.log4j.Logger;
import org.junit.Test;

import redis.clients.jedis.Jedis;

import com.alibaba.fastjson.JSON;
import com.baidu.bae.api.util.BaeEnv;

/**
 * 百度云redis, 使用json承载数据
 * @author L.cm
 * @date Jul 27, 2013 8:10:59 PM
 */
public class RedisKit {

    private static volatile Jedis jedis;
    private static final String OK = "OK";
    private static final Logger log = Logger.getLogger(RedisKit.class);
    
    // test时 注释掉
    static{
        String server = BaeEnv.getBaeHeader(BaeEnv.BAE_ENV_ADDR_REDIS_IP);
        String port_str = BaeEnv.getBaeHeader(BaeEnv.BAE_ENV_ADDR_REDIS_PORT);
        int port = Integer.parseInt(port_str);
        String host = server + ":" + port;
        String user = BaeEnv.getBaeHeader(BaeEnv.BAE_ENV_AK);
        String password = BaeEnv.getBaeHeader(BaeEnv.BAE_ENV_SK);
        String dbname = ConfigUtil.get("jedis_db");
        
        jedis = new Jedis(server, port);
        jedis.connect();
        jedis.auth(user + "-" + password + "-" + dbname);
        log.info("jedis host:\t".concat(host));
        log.info("jedis start......");
        log.info("jedis dbSize......");
    }
    // test时 注释掉
    
    static void init(Jedis jedis) {
        RedisKit.jedis = jedis;
    }
    
    /**
     * 根据key 获取 value
     * @param @param key
     * @param @return    设定文件
     * @return String    返回类型
     * @throws
     */
    public static String get(String key) {
        return jedis.get(key);
    }
    
    /**
     * 根据key 获取 map
     * @param @param key
     * @param @return    设定文件
     * @return Map<String,Object>    返回类型
     * @throws
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> getAsMap(String key) {
        String value = jedis.get(key);
        return JSON.parseObject(value, Map.class);
    }
    
    /**
     * 根据key 获取 list
     * @param @param key
     * @param @return    设定文件
     * @return Map<String,Object>    返回类型
     * @throws
     */
    @SuppressWarnings("unchecked")
    public static List<Object> getAsList(String key) {
        String value = jedis.get(key);
        return JSON.parseObject(value, List.class);
    }
    
    /**
     * 存储 key value
     * @param @param key
     * @param @param value
     * @param @return    设定文件
     * @return boolean    返回类型
     * @throws
     */
    public static boolean set(String key, String value) {
        String temp = jedis.set(key, value);
        return temp.equals(OK);
    }
    
    /**
     * 存储 key value timeout
     * @param @param key
     * @param @param value
     * @param @param timeout
     * @param @return    设定文件
     * @return boolean    返回类型
     * @throws
     */
    public static boolean set(String key, String value, int timeout) {
        String temp = jedis.setex(key, timeout, value);
        return temp.equals(OK);
    }
    
    /**
     * 存储 map jfinal 对象
     * @param @param key
     * @param @param values
     * @param @return    设定文件
     * @return boolean    返回类型
     * @throws
     */
    public static boolean setObject(String key, Object values) {
        String temp = jedis.set(key, JSON.toJSONString(values));
        return temp.equals(OK);
    }
    
    /**
     * 存储 map jfinal 对象 timeout
     * @param @param key
     * @param @param values
     * @param @param timeout
     * @param @return    设定文件
     * @return boolean    返回类型
     * @throws
     */
    public static boolean setObject(String key, Object values, int timeout) {
        String temp = jedis.setex(key, timeout, JSON.toJSONString(values));
        return temp.equals(OK);
    }
    
    @Test
    public void test() throws InterruptedException {
        jedis = new Jedis("127.0.0.1", 6379);
        RedisKit.init(jedis);
        
        List<Object> l = new ArrayList<Object>();
        l.add("hello");
        
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("1", 1);
        map.put("2", l);
        System.out.println(RedisKit.setObject("1", map, 1));
        
        Thread.sleep(2000);
        
        // some times is null
        Map<String, Object> m = RedisKit.getAsMap("1");
        
        System.out.println(m);
        
        List<Object> list = new ArrayList<Object>();
        list.add(map);
        list.add(1123123);
        list.add(1.11111);
        list.add('1');
        list.add(5L);
        
        System.out.println(RedisKit.setObject("2", list, 3));
        
        Thread.sleep(2000);
        
        // some times is null
        System.out.println(RedisKit.getAsList("2"));
    }
}
