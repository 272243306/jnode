package com.jfinal.plugin.memcache;

import net.dreamlu.utils.ConfigUtil;

import org.apache.log4j.Logger;

import com.baidu.bae.api.memcache.BaeMemcachedClient;
import com.jfinal.plugin.IPlugin;

/**
 * BAE Memchahe 插件 
 * @author L.cm
 * @date 2013-6-2 下午10:06:11
 */
public class MemCachePlugin implements IPlugin{
	
	private static final Logger log = Logger.getLogger(MemCachePlugin.class);

	private static BaeMemcachedClient cachedClient;
	
	public MemCachePlugin() {}

	@Override
	public boolean start() {
		if(null == cachedClient){
			cachedClient = new BaeMemcachedClient();
			cachedClient.setAk(ConfigUtil.get("user"));
			cachedClient.setSk(ConfigUtil.get("password"));
		}
		MemCacheKit.init(cachedClient);
		log.info("BaeMemcachedClient: init................");
		return true;
	}

	@Override
	public boolean stop() {
		cachedClient = null;
		return true;
	}
}
