package com.jfinal.plugin.activerecord.cache;

import com.jfinal.plugin.memcache.MemCacheKit;

/**
 * 百度BAE memcache
 * @author L.cm
 * @date 2013-6-2 下午8:41:25
 */
public class MemCache implements ICache {

	@Override
	public <T> T get(String key) {
		return MemCacheKit.get(key);
	}

	@Override
	public void put(String key, Object value) {
		MemCacheKit.put(key, value);
	}

	@Override
	public void put(String key, Object value, long expiry) {
		MemCacheKit.put(key, value, expiry);
	}
	
	@Override
	public void update(String key, Object value) {
		MemCacheKit.update(key, value);
	}

	@Override
	public void remove(String key) {
		MemCacheKit.remove(key);
	}

    @Override
    public boolean exists(String key) {
        return MemCacheKit.exists(key);
    }
}
