package net.dreamlu.handler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.dreamlu.model.Blog;

import com.jfinal.handler.Handler;
import com.jfinal.render.JadeXmlRender;

public class SiteMapHandler extends Handler {
    
    @Override
    public void handle(String target, HttpServletRequest request,
            HttpServletResponse response, boolean[] isHandled) {
        // 对于/sitemap-开头的定义为sitemap的请求
        if (target.startsWith("/sitemap-")) {
            String view = target.replace(".xml", ".jade");
            // 查找所有的blog估计会有耗时...
            List<Blog> blogs = Blog.dao.findAll();
            request.setAttribute("blogs", blogs);
            JadeXmlRender sitmap = new JadeXmlRender("/WEB-INF/sitemap/" + view);
            sitmap.setContext(request, response);
            sitmap.render();
            // 跳出
            isHandled[0] = true;
            return;
        }
        nextHandler.handle(target, request, response, isHandled);
    }
    
}
