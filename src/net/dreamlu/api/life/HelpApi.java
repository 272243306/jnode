package net.dreamlu.api.life;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.dreamlu.api.util.HttpUtil;
import net.dreamlu.api.util.OathConfig;

import com.alibaba.fastjson.JSON;

/**
 * 生活帮助类api
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date Jul 10, 2013 10:26:26 PM
 */
public class HelpApi {

    private static final String WEATHER_URL = "http://api.map.baidu.com/telematics/v2/weather";
    private static final String TRANSLATE_URL = "http://openapi.baidu.com/public/2.0/bmt/translate";
    private static final String TAOBAO_IP_URL = "http://ip.taobao.com/service/getIpInfo.php";
    
    public static final String DEFAULT_CITY = "北京"; // 默认城市
    
    /**
     * 百度天气api
     * @param @param params
     * @param @return    设定文件
     * @return JSONObject    返回类型
     * @throws
     * <url>http://developer.baidu.com/map/carapi-7.htm</url>
     */
    @SuppressWarnings("unchecked")
    public static List<Map<String, String>> weather(String data) throws IOException{
        // location=北京&output=json&ak=6c19c46381dd73b0b5f6a5c0c8acde42
        Map<String, String> params = new HashMap<String, String>();
        params.put("location", data);
        params.put("ak", OathConfig.getValue("lbs_ak_baidu"));
        params.put("output", "json");
        Map<String, Object> dataMap = JSON.parseObject(HttpUtil.doGet(WEATHER_URL, params), Map.class);
        if (null != dataMap && "0".equals(dataMap.get("error").toString())) {
            return (List<Map<String, String>>) dataMap.get("results");
        } else {
            return null;
        }
    }
    
    /**
     * 百度翻译api
     * @param @param data
     * @param @return    设定文件
     * @return JSONObject    返回类型
     * @throws
     * <url>http://developer.baidu.com/wiki/index.php?title=%E5%B8%AE%E5%8A%A9%E6%96%87%E6%A1%A3%E9%A6%96%E9%A1%B5/%E7%99%BE%E5%BA%A6%E7%BF%BB%E8%AF%91/%E7%BF%BB%E8%AF%91API</url>
     */
    @SuppressWarnings("unchecked")
    public static List<Map<String, String>> translate(String data) throws IOException{
        // client_id=xwKOgtKjbbrn9dOb7ZkGrAo5&q=today&from=auto&to=auto
        Map<String, String> params = new HashMap<String, String>();
        params.put("q", data);
        params.put("client_id", OathConfig.getValue("openid_baidu"));
        params.put("from", "auto");
        params.put("to", "auto");
        Map<String, Object> dataMap = JSON.parseObject(HttpUtil.doGet(TRANSLATE_URL, params), Map.class);
        if (null != dataMap && null != dataMap.get("trans_result")) {
            return (List<Map<String, String>>) dataMap.get("trans_result");
        } else {
            return null;
        }
    }

    /**
     * 淘宝ip库
     * @param @param ip
     * @param @return    设定文件
     * @return Map<String,String>    返回类型
     * @throws
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> ipData(String ip) throws IOException{
        Map<String, String> params = new HashMap<String, String>();
        params.put("ip", ip);
        String json = HttpUtil.doGet(TAOBAO_IP_URL, params);
        return JSON.parseObject(json, Map.class);
    }
}
