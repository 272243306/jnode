package net.dreamlu.api.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSONObject;

/**
 * string 帮助类
 * 
 * @author L.cm
 * @date Jun 25, 2013 11:04:37 PM
 */
public class TokenUtil {
	
	/**
	 * 参考自 qq sdk
	 * @param @param string
	 * @param @return    设定文件
	 * @return String    返回类型
	 * @throws
	 */
	public static String getAccessToken(String string) {
		String accessToken = "";
		try {
			JSONObject json = JSONObject.fromObject(string);
			if (null != json) {
				accessToken = json.getString("access_token");
			}
		} catch (Exception e) {
			Matcher m = Pattern.compile("^access_token=(\\w+)&expires_in=(\\w+)&refresh_token=(\\w+)$").matcher(string);
			if (m.find()) {
				accessToken = m.group(1);
			} else {
				Matcher m2 = Pattern.compile("^access_token=(\\w+)&expires_in=(\\w+)$").matcher(string);
				if (m2.find()) {
					accessToken = m2.group(1);
				}
			}
		}
		return accessToken;
	}
	
	/**
	 * 匹配openid
	 * @param @param string
	 * @param @return    设定文件
	 * @return String    返回类型
	 * @throws
	 */
	public static String getOpenId(String string) {
		String openid = null;
		Matcher m = Pattern.compile("\"openid\"\\s*:\\s*\"(\\w+)\"").matcher(string);
	    if (m.find())
	      openid = m.group(1);
	    return openid;
	}
	
	/**
	 * sina uid于qq分离
	 * @Title: getUid
	 * @param @param string
	 * @param @return    设定文件
	 * @return String    返回类型
	 * @throws
	 */
	public static String getUid(String string){
		JSONObject json = JSONObject.fromObject(string);
		return json.getString("uid");
	}
}
