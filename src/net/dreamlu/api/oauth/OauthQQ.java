package net.dreamlu.api.oauth;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.dreamlu.api.util.OathConfig;
import net.dreamlu.api.util.TokenUtil;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;

/**
 * OauthQQ BAE
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @author L.cm
 * @date Jun 24, 2013 9:35:23 PM
 */
public class OauthQQ extends Oauth{ 

	private static final Logger log = Logger.getLogger(OauthQQ.class);
	
	private static final String AUTH_URL = "https://graph.qq.com/oauth2.0/authorize";
    private static final String TOKEN_URL = "https://graph.qq.com/oauth2.0/token";
    private static final String TOKEN_INFO_URL = "https://graph.qq.com/oauth2.0/me";
    private static final String USER_INFO_URL = "https://graph.qq.com/user/get_user_info";

    public OauthQQ() {
		super();
		setClientId(OathConfig.getValue("openid_qq"));
		setClientSecret(OathConfig.getValue("openkey_qq"));
		setRedirectUri(OathConfig.getValue("redirect_qq"));
	}

	/**
	 * 获取授权url
	 * @param @return    设定文件
	 * @return String    返回类型
	 * @throws
	 */ 
	public String getAuthorizeUrl() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("state", "qq");
		params.put("response_type", "code");
		params.put("client_id", getClientId());
		params.put("redirect_uri", getRedirectUri());
		return super.getAuthorizeUrl(AUTH_URL, params);
	}

	/**
	 * 获取token
	 * @param @param code
	 * @param @return    设定文件
	 * @return String    返回类型
	 * @throws
	 */
	public String getTokenByCode(String code) throws IOException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("code", code);
		params.put("client_id", getClientId());
		params.put("client_secret", getClientSecret());
		params.put("grant_type", "authorization_code");
		params.put("redirect_uri", getRedirectUri());
		// access_token=FE04************************CCE2&expires_in=7776000
		String token = TokenUtil.getAccessToken(super.doGet(TOKEN_URL, params));
		log.error(token);
		return token;
	}

	/**
	 * @throws IOException 
	 * 获取TokenInfo
	 * @return    设定文件
	 * @return String    返回类型
	 * @throws
	 */
	public String getTokenInfo(String accessToken) throws IOException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", accessToken);
		// callback( {"client_id":"YOUR_APPID","openid":"YOUR_OPENID"} );
		String openid = TokenUtil.getOpenId(super.doGet(TOKEN_INFO_URL, params));
		log.error(openid);
		return openid;
	}
	
	/**
	 * @throws IOException 
	 * 获取用户信息
	 * @param accessToken
	 * @param uid
	 * @return    设定文件
	 * @return String    返回类型
	 * @throws
	 */
	public String getUserInfo(String accessToken, String uid) throws IOException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", accessToken);
		params.put("oauth_consumer_key", getClientId());
		params.put("openid", uid);
		params.put("format", "json");
		// // {"ret":0,"msg":"","nickname":"YOUR_NICK_NAME",...}
		String userinfo = super.doGet(USER_INFO_URL, params);
		log.error(userinfo);
		return userinfo;
	}
	
	/**
	 * 根据code一步获取用户信息
	 * @param @param args    设定文件
	 * @return void    返回类型
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> getUserInfoByCode(String code) throws IOException{
		String accessToken = getTokenByCode(code);
		String openId = getTokenInfo(accessToken);
		Map<String, String> dataMap = JSON.parseObject(getUserInfo(accessToken, openId), Map.class);
		dataMap.put("openid", openId);
		return dataMap;
	}
}
