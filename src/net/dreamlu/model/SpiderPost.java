package net.dreamlu.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * TODO 在此加入类描述
 * @author L.cm
 * @date 2013-06-23 23:39:57
 */
public class SpiderPost extends Model<SpiderPost> {
	
	private static final long serialVersionUID = 1L;

	public static final String TABLE_NAME = "spider_post";
	public static final String ID = "id";
	public static final String RULE_ID = "ruleId";
	public static final String TITLE = "title";
	public static final String CONTENT = "content";
	public static final String CREATE_TIME = "createTime";
	public static final String POSTED = "posted";
	
	public static final SpiderPost dao = new SpiderPost();
}