package net.dreamlu.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

/**
 * @author huxiang
 * 
 */
@SuppressWarnings({ "rawtypes" })
public abstract class DbModel<T extends DbModel> extends Model<T> {

	private static final long serialVersionUID = -6215428115177000482L;

	/**
	 * 用来针对DataTables封装的分页查询
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param select
	 * @param sqlExceptSelect
	 * @param paras
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public DataTablesModel paginateDataTables(int pageNumber, int pageSize,
			String select, String sqlExceptSelect, String sEcho, Object... paras) {

		Page<T> pages = super.paginate(pageNumber, pageSize, select,
				sqlExceptSelect, paras);

		List<List<String>> cells = new ArrayList<List<String>>();
		for (int i = 0; i < pages.getList().size(); i++) {
			T t = pages.getList().get(i);
			Map attrs = t.getAttrs();
			Set<String> key = attrs.keySet();
			List<String> cell = new ArrayList<String>();
			for (Iterator it = key.iterator(); it.hasNext();) {
				String s = (String) it.next();
				if (null != attrs.get(s)) {
					cell.add(attrs.get(s).toString());
				} else {
					cell.add("");
				}
			}
			cells.add(cell);
		}
		return new DataTablesModel(pages.getTotalRow(), pages.getTotalRow(), sEcho, cells);
	}

	/**
	 * 用来针对DataTables封装的分页查询
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param select
	 * @param sqlExceptSelect
	 * @return
	 */
	public DataTablesModel paginateDataTables(int pageNumber, int pageSize,
			String select, String sqlExceptSelect, String sEcho) {
		return this.paginateDataTables(pageNumber, pageSize, select,
				sqlExceptSelect, sEcho, new Object[0]);
	}
}
