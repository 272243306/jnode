package net.dreamlu.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * 微信用户表
 * @author L.cm
 * @date 2013-06-23 23:39:58
 * jfinal-rapid自动生成：<url>http://git.oschina.net/596392912/jfinal-rapid</url>
 */
public class WxUser extends Model<WxUser> {
	
	private static final long serialVersionUID = 1L;

	public static final String TABLE_NAME = "wx_user";
	public static final String ID = "id";
	public static final String OPENID = "openid";				// 微信openid
	public static final String NICK_NAME = "nickName";			// 用户昵称
	public static final String IS_LOGIN = "islogin";			// 是否登录状态  默认：0 登录：1
	public static final int LOGIN_N = 0;								
	public static final int LOGIN_Y = 1;
	public static final String TEMP_RULE = "temp_rule";			// 记录上一次rule
	
	public static final WxUser dao = new WxUser();
}