package net.dreamlu.helpers;

import java.sql.Timestamp;
import java.util.List;

import net.dreamlu.kit.StringsKit;
import net.dreamlu.model.Blog;
import net.dreamlu.model.BlogTag;
import net.dreamlu.model.Tags;
import net.dreamlu.utils.DateUtil;
import net.dreamlu.utils.HtmlFilter;

import com.jfinal.kit.StringKit;


/**
 * Jade4J 标签 帮助
 * @author L.cm
 * @param <T>
 * @date Aug 29, 2013 6:58:35 PM
 */
public class JadeHelper {
    
    // use #{jTag.baseFormat(XXXX)}
    public String baseFormat(Timestamp tamp) {
        return DateUtil.format(tamp);
    }
    
    // use #{jTag.format(XXXX)}
    public String format(Timestamp tamp) {
        return DateUtil.formatCn(tamp);
    }
    
    // use #{jTag.formatRss(XXXX)}
    public String formatRss(Timestamp tamp) {
        return DateUtil.formartRss(tamp);
    }
    
    // 清除字符串中的html标签
    public String filterText(String string) {
        return HtmlFilter.getText(string);
    }
    
    // 清除字符串中的html标签并截取
    public String filterSubText(String string, int length) {
        return StringsKit.subCn(HtmlFilter.getText(string), length, "...");
    }
    
    // 标记关键字
    public String markKeywords(String string, int length, String keywords) {
        if (StringKit.notBlank(keywords)) {
            return HtmlFilter.markKeywods(keywords, filterSubText(string, length));
        } else {
            return filterSubText(string, length);
        }
    }
    
    // 标签帮助，根据博文查找该博文的表情集合
    public String tags(Integer blogid) {
        StringBuilder html = new StringBuilder();
        List<BlogTag> list = BlogTag.dao.findCacheListByBlogId(blogid);
        if(null != list && list.size() > 0) {
            html.append("<span class=\"mt_icon\"></span>");
        }
        String temp = "<a title=\"{}\" href=\"/tags/{}\">{}</a>";
        for (int i = 0; i < list.size(); i++) {
            BlogTag blogTag = list.get(i);
            html.append(temp.replace("{}", blogTag.getStr(Tags.TAG_NAME)));
            if (i != list.size() - 1 ) {
                html.append(",");
            }
        }
        return html.toString();
    }
    
    // 热门标签 默认最多22个
    public List<Tags> hostTags() {
        return Tags.dao.findHostTags(15);
    }
    
    // 热门话题 5条
    public List<Blog> topBlogs() {
        return Blog.dao.findHotList(5);
    }
    
    // 最新博文 5条
    public List<Blog> lateBlogs() {
        return Blog.dao.findLateList(5);
    }
}