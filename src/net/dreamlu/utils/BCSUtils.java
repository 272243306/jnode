package net.dreamlu.utils;

import java.io.File;
import java.io.InputStream;

import net.dreamlu.config.Consts;

import org.apache.log4j.Logger;

import com.baidu.inf.iis.bcs.BaiduBCS;
import com.baidu.inf.iis.bcs.auth.BCSCredentials;
import com.baidu.inf.iis.bcs.model.ObjectMetadata;
import com.baidu.inf.iis.bcs.model.X_BS_ACL;
import com.baidu.inf.iis.bcs.request.CreateBucketRequest;
import com.baidu.inf.iis.bcs.request.PutObjectRequest;
import com.baidu.inf.iis.bcs.response.BaiduBCSResponse;

/**
 * 百度云储存上传
 * @author L.cm
 * @date 2013-5-22 下午2:19:05
 * BAE 常用服务：<url> http://developer.baidu.com/wiki/index.php?title=docs/cplat/rt/demo</url>
 */
public class BCSUtils {

    private static final Logger log = Logger.getLogger(BCSUtils.class);
    // ----------------------------------------
    public static String bucket = ConfigUtil.get("bucket");
    public static boolean hasBucket = false;

    public static BaiduBCS getBaiduBCS (){
        BCSCredentials credentials = new BCSCredentials(Consts.AK, Consts.SK);
        BaiduBCS baiduBCS = new BaiduBCS(credentials, "bcs.duapp.com");
        baiduBCS.setDefaultEncoding("UTF-8"); // Default UTF-8 
        return baiduBCS;
    }
    
    /**
     * 新建一个公开的 bucket
     * @param @param baiduBCS    设定文件
     * @return void    返回类型
     * @throws
     */
    private static void createBucket(BaiduBCS baiduBCS) {
        if (!hasBucket) {
            try {
                baiduBCS.createBucket(new CreateBucketRequest(bucket, X_BS_ACL.PublicRead));
            } catch (Exception e) {
                log.error(e);
                hasBucket = true;
            }
        }
    }

    /**
     * bcs-sdk-java_1.4.3.zip
     * 
     * url: http://developer.baidu.com/wiki/index.php?title=docs/cplat/stor/sdk
     * 
     * 说明 更多的 请根据 /bcs-sdk-java_1.4.3/sample/Sample.java 更改
     */
    
    /**
     * 直接上传文件 
     * @param @param file
     * @param @return    设定文件
     * @return boolean    返回类型
     * @throws
     */
    public static boolean uploadByFile (File file, String fileName){
        BaiduBCS baiduBCS = getBaiduBCS();
        try {
            createBucket(baiduBCS);
            String object = "/" + fileName;
            PutObjectRequest request = new PutObjectRequest(bucket, object, file);
            ObjectMetadata metadata = new ObjectMetadata();
            // 设置成公开读
            request.setAcl(X_BS_ACL.PublicRead);
            request.setMetadata(metadata);
            BaiduBCSResponse<ObjectMetadata> response = baiduBCS.putObject(request);
            ObjectMetadata objectMetadata = response.getResult();
            log.info("x-bs-request-id: " + response.getRequestId());
            log.info(objectMetadata);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            return false;
        }
    }
    
    /**
     * 百度云文件上传
     * @param @param content
     * @param @param contentType
     * @param @param length
     * @param @param last
     * @param @return    设定文件
     * @return boolean    返回类型
     * @throws
     */
    public static boolean uploadByInputStream(InputStream content, String contentType, long length, String fileName) {
        BaiduBCS baiduBCS = getBaiduBCS();
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(contentType);
        objectMetadata.setContentLength(length);
        // 设置成公开读
        objectMetadata.setHeader("x-bs-acl", "public-read");
        String object = "/" + fileName;
        PutObjectRequest request = new PutObjectRequest(bucket, object, content, objectMetadata);
        ObjectMetadata result = baiduBCS.putObject(request).getResult();
        System.out.println(result.getContentMD5());
        log.info(result);
        return true;
    }
}
