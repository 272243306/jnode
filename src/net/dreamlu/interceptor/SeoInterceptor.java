package net.dreamlu.interceptor;

import net.dreamlu.utils.ConfigUtil;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.kit.StringKit;

/**
 * SEO 搜索引擎title和关键字处理
 * @author L.cm
 * @date 2013-5-15 下午2:00:42
 */
public class SeoInterceptor implements Interceptor {

    @Override
    public void intercept(ActionInvocation ai) {
        ai.invoke();
        String title = ai.getController().getAttr("title");
        if(StringKit.isBlank(title) && !ai.getControllerKey().startsWith("/admin")){
            ai.getController().setAttr("title", ConfigUtil.get("title"));
        }
    }
    
}
