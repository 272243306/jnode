package net.dreamlu.controller.api;

import net.dreamlu.api.oauth.OauthSina;

import org.apache.log4j.Logger;

import com.jfinal.core.Controller;

/**
 * sina登录api
 * @author L.cm
 * @date 2013-5-14 下午5:08:12
 */
public class SinaLogin extends Controller {
	
    private static final Logger log = Logger.getLogger(SinaLogin.class);
    
	public void index() {
		try {
			OauthSina sina = new OauthSina();
			redirect(sina.getAuthorizeUrl());
		} catch (Exception e) {
			log.error(e);
			redirect("/");
		}
	}

	public void callback() {
	    try {
	    	OauthSina sina = new OauthSina();
//	    	String userInfo = sina.getUserInfoByCode(getPara("code"));
	    	String userInfo = sina.getTokenByCode(getPara("code"));
			renderText(userInfo);
    	    //String accessToken = sina.getTokenByCode(getPara("code"));
    	    
    	    /*
            Account am = new Account();
            am.client.setToken(accessToken);
            String openid = am.getUid().getString("uid");
            String type = "sina";
            WBLogin login = WBLogin.dao.findByOpenID(openid, type);
            if(null == login) {
            	login = new WBLogin();
            	Users um = new Users();
                um.client.setToken(accessToken);
                weibo4j.model.User user = um.showUserById(openid);
                String name = user.getName();
                login.set(WBLogin.OPENID, openid).set(WBLogin.TYPE, type).set(WBLogin.CREATETIME, new Date()).set(WBLogin.NICKNAME, name).save();
            }
            if(null == login.getInt(WBLogin.USERID) && null != login.getInt(WBLogin.ID)) {
            	// 跳转到绑定页
            	setAttr("name", login.getStr(WBLogin.NICKNAME));
            	setAttr("type", login.getStr(WBLogin.TYPE));
            	setAttr("id", login.getInt(WBLogin.ID));
            	render("binding");
            	return;
            }
            User user = User.dao.findById(login.getInt(WBLogin.USERID));
            setSessionAttr("user", user);
        	redirect("/admin");
        	*/
	    }catch(Exception e){
	    	log.error(e);
	        redirect("/");
	    }
	}
}
