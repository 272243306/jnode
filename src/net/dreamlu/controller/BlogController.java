package net.dreamlu.controller;

import net.dreamlu.model.Blog;

import com.jfinal.core.Controller;

/**
 * 博文详情
 * @author L.cm
 * @date 2013-5-14 下午5:08:12
 */
public class BlogController extends Controller{
    
    public void index() {
        int id = getParaToInt(0, 1);
        // 读写分离
        Blog blog = Blog.dao.findFallById(id);
        Blog db_blog = Blog.dao.findById(id);
        db_blog.set(Blog.VIEW_COUNT, db_blog.getInt(Blog.VIEW_COUNT) + 1).update();
        setAttr("blog", blog);
        setAttr("title", blog.getStr(Blog.TITLE));
        setAttr("typeName", Blog.TYPE_NAME[blog.getInt(Blog.BLOG_TYPE)]);
        setAttr("typeUrl", Blog.TYPE_URL[blog.getInt(Blog.BLOG_TYPE)]);
        render("blog");
    }
}
