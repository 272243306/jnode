package net.dreamlu.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.dreamlu.kit.ImageKit;
import net.dreamlu.kit.MailKit;

import com.jfinal.core.Controller;
import com.jfinal.ext.plugin.redis.RedisKit;

/**
 * 对BAE的一些功能性的测试
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date Oct 3, 2013 8:46:47 AM
 */
public class TestController extends Controller {

    // redis 测试
    public void t() {
        List<Object> l = new ArrayList<Object>();
        l.add("hello");
        
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("1", 1);
        map.put("2", 2);
        
        l.add(map);
        RedisKit.setObject("test", l, 5);
        renderJson(RedisKit.getAsList("test"));
    }

    // 验证码测试
    public void tt() {
        Map<String, String> vcodeMap = ImageKit.cenerateVCode();
        setSessionAttr("vcode_secret", vcodeMap.get("secret"));
        // {"secret":"","imgurl":""}
        renderJson(vcodeMap);
    }

    // 验证码测试
    public void tv() {
        String input = getPara("code", "");
        String secret = getSessionAttr("vcode_secret");
        // {"reason":"success","status":"0"} 
        // status == '0' 正确 
        renderJson(ImageKit.verifyVCode(input, secret));
    }

    public void mail() {
        String email = getPara("email", "453871784@qq.com");
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("user", "大黄");
        model.put("baseUrl", "http://dreamlu.net/");
        model.put("verifyUrl", "http://dreamlu.net/");
        MailKit.sendTemplateEmail("测试邮件", email, model, "signup_send.jade");
        renderNull();
    }
}
