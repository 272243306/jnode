package net.dreamlu.controller.admin;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.dreamlu.config.Consts;
import net.dreamlu.interceptor.AdminInterceptor;
import net.dreamlu.model.Blog;
import net.dreamlu.model.BlogTag;
import net.dreamlu.model.User;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.StringKit;

/**
 * 后台博文管理
 * @author L.cm
 * @date 2013-5-31 下午9:47:55
 */
@Before(AdminInterceptor.class)
public class AdminBlogController extends Controller {
    /**
     * 后台 blog 列表页
     * @param     设定文件
     * @return void    返回类型
     * @throws
     */
    public void index() {}
    
    /**
     * 后台 blog 列表
     * @param     设定文件
     * @return void    返回类型
     * @throws
     */
    public void list_json() {
        int iDisplayStart = getParaToInt("iDisplayStart", 0);
        int pageSize = getParaToInt("iDisplayLength", 10);
        int pageNum =  iDisplayStart / pageSize + 1;
        String sEcho = getPara("sEcho", "1");
        String search = getPara("sSearch");
        renderJson(Blog.dao.pageDataTables(pageNum, pageSize, sEcho, search));
    }
    
    /**
     * 后台写博或跟新
     * @param     设定文件
     * @return void    返回类型
     * @throws
     */
    public void add_edit() {
        Integer id = getParaToInt();
        if(null != id){
            Blog blog = Blog.dao.findById(id);
            List<BlogTag> list = BlogTag.dao.findByBlogId(blog.getInt(Blog.ID));
            setAttr("blog", blog);
            setAttr("tags", list);
        }
    }

    /**
     * 保存或更新博客
     * @param     设定文件
     * @return void    返回类型
     * @throws
     */
    public void save_update() {
        Blog blog = getModel(Blog.class);
        boolean state = false;
        if(null == blog.getInt(Blog.ID)){
            User user = getSessionAttr("user");
            blog.set(Blog.USER_ID, user.getInt(User.ID));
            state = blog.saveBlog();
        } else {
            state = blog.updateBlog();
        }
        // 同步标签
        Integer blogId = blog.get(Blog.ID);
        List<Integer> list = BlogTag.dao.findIdsByBlogId(blogId);
        Integer[] tags = getParaValuesToInt("tags");
        // 1.没有标签
        if (null == list && null == tags) {
            renderJson(Consts.AJAX_STATUS, state);
            return;
        }
        // 2.数据库没有该博文的标签
        if (null == list) {
            BlogTag.dao.saveAllTags(blogId, tags);
            renderJson(Consts.AJAX_STATUS, state);
            return;
        }
        // 3.删除完tags
        if (null == tags) {
            BlogTag.dao.removeAllTags(blogId);
            renderJson(Consts.AJAX_STATUS, state);
            return;
        }
        List<Integer> tagsList = Arrays.asList(tags);
        // 4.没有改变
        if (list.size() == tagsList.size() && list.containsAll(tagsList)) {
            renderJson(Consts.AJAX_STATUS, state);
            return;
        }
        // 5.交叉部分 因为标签不可能那么多所以整的比较简单
        Set<Integer> common = new HashSet<Integer>();
        for (Integer key : list) {
            if(tagsList.contains(key)) {
                common.add(key);
            }
        }
        Set<Integer> newTags = new HashSet<Integer>();
        for (Integer key : tagsList) {
            if (!common.contains(key)) {
                newTags.add(key);
            }
        }
        if (newTags.size() > 0) {
            BlogTag.dao.saveAllTags(blogId, newTags.toArray());
        }
        Set<Integer> oldTags = new HashSet<Integer>();
        for (Integer key : list) {
            if (!common.contains(key)) {
                oldTags.add(key);
            }
        }
        if (oldTags.size() > 0) {
            BlogTag.dao.removeTags(blogId, oldTags.toArray());
        }
        renderJson(Consts.AJAX_STATUS, state);
    }

    /**
     * 删除或显示博文
     * @param     设定文件
     * @return void    返回类型
     * @throws
     */
    public void delete_show() {
        Blog blog = Blog.dao.findById(getParaToInt());
        boolean temp = false;
        if(StringKit.notNull(blog)){
            int status = blog.getInt(Blog.DEL_STATUS) == Blog.DEL_N ? Blog.DEL_Y : Blog.DEL_N;
            temp = blog.set(Blog.DEL_STATUS, status).update();
        } 
        renderJson(Consts.AJAX_STATUS, temp);
    }
}
