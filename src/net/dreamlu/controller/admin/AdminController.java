package net.dreamlu.controller.admin;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.dreamlu.config.Consts;
import net.dreamlu.interceptor.AdminInterceptor;
import net.dreamlu.kit.ImageKit;
import net.dreamlu.kit.MailKit;
import net.dreamlu.kit.StringsKit;
import net.dreamlu.model.Blog;
import net.dreamlu.model.MailVerify;
import net.dreamlu.model.Options;
import net.dreamlu.model.User;
import net.dreamlu.model.WBLogin;
import net.dreamlu.utils.BCSUtils;
import net.dreamlu.validator.EmailValidator;
import net.dreamlu.validator.PwdValidator;

import org.apache.log4j.Logger;

import com.baidu.bae.api.util.BaeEnv;
import com.jfinal.aop.Before;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.aop.ClearLayer;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StringKit;
import com.jfinal.render.JsonRender;
import com.jfinal.upload.UploadFile;

/**
 * 网站后台
 * @author L.cm
 * @date 2013-6-1 下午5:29:58
 */
@Before(AdminInterceptor.class)
public class AdminController extends Controller{

    private static final Logger log = Logger.getLogger(AdminController.class);
    private static final String IMG_TYPE = ".jpg|.jepg|.gif|.png|.bmp";
    
    /**
     * 后台首页,博文热度图
     */
    public void index() {
        List<List<Integer>> hostBlogList = Blog.dao.hostBlogList();
        setAttr("data", JsonKit.listToJson(hostBlogList, 2));
    }

    /**
     * 安装
     * @param     设定文件
     * @return void    返回类型
     * @throws
     */
    @ClearInterceptor(ClearLayer.ALL)
    @Before({EmailValidator.class, PwdValidator.class})
    public void install() {
    	String email = getPara("email");
        String pwd = getPara("pwd");
        boolean temp = User.dao.saveUser(email, pwd);
        if (temp) {
        	Consts.IS_INSTALL = true;
        }
        renderJson(Consts.AJAX_STATUS, temp);
    }
    
    /**
     * 登录
     */
    @ClearInterceptor
    @Before({EmailValidator.class, PwdValidator.class})
    public void session() {
        String email = getPara("email");
        String pwd = getPara("pwd");
        Integer remember = getParaToInt("remember", 0);
        User user = User.dao.login(email, StringsKit.pwdEncrypt(pwd));
        if(StringKit.notNull(user)) {
            if(remember == 1){
                String code = StringsKit.cookieEncrypt(email, pwd);
                setCookie("userId", code, 60 * 60 * 24 * 30, "/", Consts.DOMAIN_COOKIE);
            }
            user.updateUser();
            setSessionAttr(Consts.USER_SESSION, user);
            setAttr(Consts.AJAX_STATUS, 0);
        }else {
            setAttr(Consts.AJAX_STATUS, 1);
        }
        renderJson(new String[]{Consts.AJAX_STATUS});
    }
    
    /**
     * 用户绑定
     */
    @ClearInterceptor
    @Before({EmailValidator.class})
    public void binding() {
        String email = getPara("email");
        int wbId = getParaToInt("id");
        WBLogin wb = WBLogin.dao.findById(wbId);
        if (null == wb) {
            renderJson(Consts.AJAX_STATUS, Consts.AJAX_N);
            return;
        }
        User user = User.dao.findByEmail(email);
        if (null == user) {
        	user = new User();
            user.saveWbUser(email, wb);
        }
        boolean status = wb.set(WBLogin.USERID, user.getInt(User.ID)).update();
        if (status){
            MailVerify verify = new MailVerify();
            // 设置成WBLogin
            verify.set(MailVerify.USER_ID, wb.getInt(WBLogin.ID));
            status = MailVerify.dao.createVerify(verify);
            // 邮件校验
            Map<String, Object> model = new HashMap<String, Object>();
            Options options = Options.dao.findByCache();
            model.put("options", options);
            model.put("verifyUrl",  "/finish?code=" + verify.getStr(MailVerify.VERIFY_CODE));
            // options.SITE_URL, options.wb_qq, options.wb_sina
            MailKit.sendTemplateEmail("DreamLu博客邮箱绑定", email, model, "signup_send.jade");
        }
        renderJson(Consts.AJAX_STATUS, status ? Consts.AJAX_Y : Consts.AJAX_N);
    }

    /**
     * kindedit 图片上传 Bae 环境
     */
    @Before(POST.class)
    public void editor(){
        // BAE 临时目录上传
        String tmpfsPath = BaeEnv.getTmpfsPath();
        try {
            UploadFile file = getFile("imgFile", tmpfsPath, 1024 * 1024);
            String oldName = file.getFileName();
            String fileType = oldName.substring(oldName.lastIndexOf("."), oldName.length());
            if(IMG_TYPE.indexOf(fileType.toLowerCase()) == -1){
                 setAttr("error", 1);
                 setAttr("message", "上传出错，请检查图片格式！");
                 render(new JsonRender(new String[]{"error", "message"}).forIE());
                 return;
            }
            String newName = System.currentTimeMillis() + fileType;
            // 第一次上传文件
            BCSUtils.uploadByFile(file.getFile(), newName);
            String url = "http://bcs.duapp.com/" + BCSUtils.bucket + "/" + newName;
            byte[] bs = ImageKit.watermark(url, Consts.DOMAIN_NAME); // 水印域名
            // 第二次水印之后重新上传
            BCSUtils.uploadByInputStream(new ByteArrayInputStream(bs), "image/jpeg", bs.length, newName);
            setAttr("error", 0);
            setAttr("url", url);
            render(new JsonRender(new String[]{"error", "url"}).forIE());
        } catch (Exception e) {
            log.error(e);
            setAttr("error", 1);
            setAttr("message", "上传出错，请稍候再试！");
            render(new JsonRender(new String[]{"error", "message"}).forIE());
        }
    }
}
