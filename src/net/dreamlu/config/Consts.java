package net.dreamlu.config;

import net.dreamlu.utils.ConfigUtil;

/**
 * 系统常量
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2013-7-1 上午9:28:16
 */
public class Consts {

	// 缓存时间 20分钟
	public static final long CACHE_TIME_MINI = 1200000L;
	// 缓存时间 1小时
	public static final long CACHE_TIME_MAX = 3600000L;
	// Http缓存时间 一个月
	public static final long HTTP_CACHE_TIME = 2592000L;
	// 用户session key
	public static final String USER_SESSION = "user";
	// 分页大小
	public static final int BLOG_PAGE_SIZE = 8;
	// 百度云 ak，sk
	public static String AK = ConfigUtil.get("bae.user");
	public static String SK = ConfigUtil.get("bae.password");
	public static String DOMAIN_COOKIE = ConfigUtil.get("domain");
	public static String DOMAIN_NAME   = ConfigUtil.get("domain.name");
	public static String DOMAIN_URL    = "http://".concat(DOMAIN_NAME);
	// 安装状态
	public static boolean IS_INSTALL = false;
	
	// ajax 状态
	public static final String AJAX_STATUS = "status";
	public static final int AJAX_Y = 0; //OK
	public static final int AJAX_N = 1; //no
	public static final int AJAX_S = 2; //权限
	public static final int AJAX_O = 3; //other 其他错误
}
