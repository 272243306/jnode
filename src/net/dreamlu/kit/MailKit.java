package net.dreamlu.kit;

import java.util.Map;

import net.dreamlu.utils.ConfigUtil;

import com.baidu.bae.api.bcms.client.BCMSRestClient;
import com.baidu.bae.api.bcms.model.concrete.MailRequest;
import com.jfinal.kit.PathKit;

import de.neuland.jade4j.Jade4J;
import de.neuland.jade4j.template.JadeTemplate;

/**
 * BAE 邮件发送
 * @author L.cm
 * @date 2013-6-2 下午3:15:55
 * <url>http://developer.baidu.com/bae/bms/list/</url>
 */
public class MailKit {
	
	/**
	 * BAE 发送邮件
	 * @param from
	 * @param subject
	 * @param content
	 * @param to
	 */
	public static void send(String subject, String content, String to) {
		BCMSRestClient bcms = new BCMSRestClient();
		MailRequest mailRequest = new MailRequest();
		mailRequest.setQueueName(ConfigUtil.get("queue"));
		mailRequest.setSubject(subject);
		mailRequest.setMessage(content);
		mailRequest.addMailAddress(to);
		bcms.mail(mailRequest);
	}

	/**
	 * 发送jade模板邮件
	 * @Title: sendTemplateEmail
	 * @param @param subject
	 * @param @param meilTo
	 * @param @param model
	 * @param @param tempname    设定文件
	 * @return void    返回类型
	 * @throws
	 */
	public static boolean sendTemplateEmail(String subject, String to, Map<String, Object> model, String tempname){
		try {
			JadeTemplate template = Jade4J.getTemplate(PathKit.getWebRootPath() + "/WEB-INF/mail_template/" + tempname);
			String html = Jade4J.render(template, model);
			send(subject, html, to);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}