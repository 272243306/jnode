package net.dreamlu.kit;

import java.util.Map;

import com.baidu.bae.api.factory.BaeFactory;
import com.baidu.bae.api.image.Annotate;
import com.baidu.bae.api.image.BaeImageService;
import com.baidu.bae.api.image.Image;
import com.baidu.bae.api.image.ImageConstant;
import com.baidu.bae.api.image.QRCode;
import com.baidu.bae.api.image.VCode;

/**
 * BAE 图片处理
 * @author L.cm
 * @date 2013-6-4 下午9:07:57
 * <url>http://developer.baidu.com/wiki/index.php?title=docs/cplat/rt/java/image</url>
 */
public class ImageKit {
	
	private final static int FONT_SIZE = 18;
	
	/**
	 * 生成验证码
	 * @param @return    设定文件
	 * @return Map<String,String>    返回类型
	 * key: imgurl   secret
	 * @throws
	 */
	public static Map<String, String> cenerateVCode() {
		BaeImageService service = BaeFactory.getBaeImageService();
		return service.generateVCode(new VCode());
	}
	
	/**
	 * 验证码校验
	 * @param input
	 * @param  secret
	 * @param @throws Exception    设定文件
	 * @return Map<String,String>    返回类型
	 * key: status   reason
	 * @throws
	 */
	public static Map<String, String> verifyVCode(String input, String secret) {
		BaeImageService service = BaeFactory.getBaeImageService();
		VCode vc = new VCode();
		vc.setSecret(secret);
		vc.setInput(input);
		return service.verifyVCode(vc);
	}
	
	/**
	 * 图片水印文字
	 * @param url
	 * @param text
	 * @param wideth
	 * @param height
	 * @return byte[]    返回类型
	 * @throws
	 */
	public static byte[] watermark(String url, String text){
		BaeImageService service = BaeFactory.getBaeImageService();
		Image image = new Image(url);
		//创建文字水印功能对象
		Annotate annotate = new Annotate(text);
		annotate.setFont(ImageConstant.FONT_SUN, FONT_SIZE,"9b9b9b");
		annotate.setOpacity(0.4f);	// 透明度
		annotate.setQuality(86);	// 图片质量 经测试86 图片效果 和文件大小相对合适
//		annotate.setPos(wideth - (getLength(text) * FONT_SIZE), height - FONT_SIZE);
		// 本想获取图片的大小 水印在右下角 结果api被封杀了
		annotate.setPos(2, 1);
		return service.applyAnnotate(image, annotate);
	}
	
	/**
	 * 二维码生成
	 * @param @param text
	 * @param @param size
	 * @param @return    设定文件
	 * @return byte[]    返回类型
	 * @throws
	 */
	public static byte[] qRcode(String text, int size){
	    BaeImageService service = BaeFactory.getBaeImageService();
	    QRCode qrcode = new QRCode(text);
	    qrcode.setSize(10);
	    qrcode.setBackground("FFFFFF");
	    return service.applyQRCode(qrcode);
	}
}
